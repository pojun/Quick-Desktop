﻿namespace webquicker;

import wsock;
import wsock.err;
import inet.http

server = class {

	ctor (maxThreadCount = 20) {
		this.maxThreadCount = maxThreadCount;
		..thread.set("threadCount", 0);
		..table.gc(this, "stop")
	}
	
	start = function (port) {
		port = type(port) == type.number ? port : 80;
		
		this.stop();
		
		// 初始化 Winsock
		if (!..wsock.open()) {
			error(..wsock.err.lasterr(), 2);
		}
		
		// 创建套接字
		this.hSocket = ..wsock.socket(0x2/*_AF_INET*/, 0x1/*_SOCK_STREAM*/, 0x6/*_IPPROTO_TCP*/);
		if (this.hSocket == _INVALID_SOCKET) {
			..wsock.close();
			error("创建套接字失败!", 2);
		}
		
		// 绑定
		var service = ..wsock.sockaddr_in();
		service.sin_family = 0x2/*_AF_INET*/;
		service.sin_addr.S_un.S_addr = ..wsock.htonl(0x0/*_INADDR_ANY*/);
		service.sin_port = ..wsock.htons(port);
		if (..wsock.bind(this.hSocket, service, ..raw.sizeof(service)) == -1/*_SOCKET_ERROR*/) {
			..wsock.closesocket(hSocket);
			..wsock.close();
			error("绑定端口失败,请检查端口是否被占用!", 2);
		}
		
		// 监听
		if (..wsock.listen(this.hSocket, 10) == -1/*_SOCKET_ERROR*/) {
			..wsock.closesocket(this.hSocket);
			..wsock.close();
			error("监听失败,请检查端口是否被占用!", 2);
		}
		
		// 创建响应线程
		this.hThread = ..thread.create(function (hSocket, func, maxThreadCount, settings) {
			import wsock;
			import thread.manage
			// ..wsock.ioctlsocket(hSocket, 0x8004667e, 1);
			..thread.set("thread_stop", false);
			var tm = thread.manage(maxThreadCount)
			
			while (true) {
				if (..thread.get("thread_stop")) {
					tm.waitClose()
					break;
				}
				var hAcceptSocket = ..wsock.accept(hSocket, 0, 0);
				if (hAcceptSocket != _INVALID_SOCKET) {
					tm.createLite(function (hSocket, func, settings) {
						import wsock;
						import thread.command

						..thread.set("threadCount", ..thread.get("threadCount") + 1); // 递加线程数
						try {
							func(hSocket, settings);

						} catch (e) {
							thread.command.post("onError", tostring(e))
						}
						func = null;
						..wsock.closesocket(hSocket);
						hSocket = null;
						collectgarbage("collect")
						..thread.set("threadCount", ..thread.get("threadCount") - 1);
						 
					}, hAcceptSocket, func, settings)
				}
			}//end while
		}, this.hSocket, this.accept, this.maxThreadCount, ..settings);
	}
	
	stop = function () {
		if (this.hThread) {
			..thread.set("thread_stop", true);
			var http = ..inet.http()
			http.get("http://127.0.0.1:"+..settings["port"]) //发一个URL请求，退出accept线程
			http.close()
			if(!..thread.wait(this.hThread, 2000))
				..thread.terminate(this.hThread, 0);
			..raw.closehandle(this.hThread);
			..wsock.closesocket(this.hSocket);
			..wsock.close();
			this.hThread = null;
			..thread.command.send("onClose")
		}
		return true; 
	}
	
// 处理函数（在子线程中）
/*{{*/
	accept = function (hSocket, settings) {
		import fsys;
		import fsys.file;
		import webquicker.parser;
		import fiber
		import string.html
		
		var data = "";
		var len, buffer;
	
		while (!string.endWith(data, '\r\n\r\n') && !string.endWith(data, '\n\n')) {
			len, buffer = wsock.recv(hSocket, 1, 1, 0);
			if (len <= 0) {
				break;
			}
			data = data + buffer;
		}
		
		var info = webquicker.parser.parseRequest(data);
		
		// 如果有附加的数据(POST)
		var contentLength = tonumber(info.header[["Content-Length"]] or "0");
		if (contentLength) {
			len, buffer = wsock.recv(hSocket, contentLength, contentLength, 0);
			data = data + buffer;
			info = webquicker.parser.parseRequest(data);
		}
	
		response = webquicker.parser.response(hSocket, self);
		request = info;
		
		// 错误处理
		err_404 = function () {
			response.status = "404 File Not Found";
			response.body = "<html><head><title>File Not Found</title></head><body><h1>404 File Not Found</h1>";
			response.body += "</body></html>";
		}
		
		err_500 = function (msg) {
			response.status = "500 Internal Server Error";
			response.body = "<html><head><title>Script Error</title></head><body><h1>Script Error</h1>";
			response.body += "<hr/><p><strong>Message: </strong>" + string.html.fromText(msg) + "</p></body></html>";
		}
		
		// 执行 aau
		loadPage = function (path) {
			var realPath = string.sub(path, 2, 2) == ":" ? path : fsys.joinpath(io.fullpath(settings["wwwroot"]), path + ".aau");
			realPath = string.replace(realPath, "\\", "/");
			realPath = string.replace(realPath, "/+", "/");
			if (!io.exist(realPath)) {
				err_404();
			} else {
				var preFun, err1 = loadcode("/res/standard.aau")
				var func, err2 = loadcode(realPath);
				if (preFun and func) {
					var parentDir = fsys.getParentDir(realPath);
					var preCurDir = fsys.getCurDir();
					fsys.setCurDir(parentDir);
					var ret = { fiber.resume( fiber.create(preFun, settings["wwwroot"]), realPath, settings ) };
					if (!ret[1] && ret[2] !== "") err_500(ret[2] or "Unknown");
					
					ret = { fiber.resume( fiber.create(func, settings["wwwroot"]), realPath ,hSocket) };
					fsys.setCurDir(preCurDir);
					if (!ret[1] && ret[2] !== "") err_500(ret[2] or "Unknown");
					
				} else {
					err_500( (err1+err2)  or "Unknown");
				}
			}
		}
		
		var flag = false;
		
		if (info.requireRedirect) {
			response.redirect(info.url);
			
		} else {
			var fullpath = fsys.joinpath(io.fullpath(settings["wwwroot"]), info.url);
			
			if (!string.endWith(info.url, "/") && fsys.isDir(fullpath)) {
				response.redirect_301(info.url + "/");
				response.flush();
			}
			elseif (string.endWith(info.url, "/")) {
				// 自动寻找首页，严格按照顺序
				for (i = 1; #webquicker.parser.indexes) {
					if (io.exist(fullpath + webquicker.parser.indexes[i])) {
						info.url += webquicker.parser.indexes[i];
						break;
					}
				}
			}
			
			fullpath = fsys.joinpath(io.fullpath(settings["wwwroot"]), info.url);

			// 对于不存在的文件，返回 404 错误
			if (!string.startWith(fullpath, io.fullpath(settings["wwwroot"])) ||
				 !io.exist(fullpath) || fsys.isDir(fullpath)) 
			{
				err_404();
				
			} else {
				var tpath = io.splitpath(info.url);
				if (string.lower(tpath.ext) == ".aau") {	//脚本文件
					// 脚本文件，执行
					loadPage(tpath.dir + tpath.name);
					
				} else {	//资源文件

					// MIME
					response.header["Content-Type"] = "application/stream";
					for (k, v in webquicker.parser.mime) {
						if (tpath.ext == "." + k) {
							response.header["Content-Type"] = v;
						}
					}
						
					// 打开文件
					var file = fsys.file(fullpath);
					if (file) {
						// 最后修改日期
						var fileTime = file.getFileTimes();
						var t = fileTime.write;
						t.format = "%a, %d %b %Y %H:%M:%S GMT"; // 格式化为 GMT 时间
						
						var tSince;
						if (request.header[["If-Modified-Since"]]) {
							try {
								tSince = time(request.header[["If-Modified-Since"]], "%a, %d %b %Y %H:%M:%S GMT");
								tSince.addhour(8); // GMT +8:00
							} catch (e) {
							
							}
						}
						
						if (tSince && tSince >= t) {
							response.status = "304 Not Modified";
							response.body = "";
						} else {
							t.addhour(-8); // GMT +0:00
							response.header["Last-Modified"] = tostring(t);
							// 发送回复头
							var bytesTotal = file.seek("end");
							var responseHeader = response.makeResponseHeader(bytesTotal);
							wsock.send(hSocket, responseHeader, #responseHeader, 0);
							file.seek("set");
							// 发送文件
							var bytesSent = 0;
							while (bytesSent < bytesTotal) {
								var bytesRead = math.min(bytesTotal - bytesSent, 102400);
								var str = file.read(bytesRead);
								wsock.send(hSocket, str, #str, 0);
								bytesSent += bytesRead;
							}
							file.close();	
							
							flag = true;
						}
					}
				}
			}
		}
		
		// 如果非后台脚本文件，则采用一次性发送的形式
		if (!flag && !response.closed) {
			response.flush();
		}
	}
/*}}*/
		
}